extends Node2D

func _on_Area2D_body_entered(body):
	if body.name == "MainChar":
		if Stats.loop >= 10:
			Stats.power += 100
			get_tree().change_scene("res://Scenes/MainGame.tscn")
		Stats.loop += 1
		if Stats.loop <= 10:
			get_tree().change_scene("res://Scenes/Loop.tscn")

func _on_Area2D2_body_entered(body):
	if body.name == "MainChar":
		get_tree().change_scene("res://Scenes/MainGame.tscn")
