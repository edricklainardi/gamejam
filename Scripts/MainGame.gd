extends Node2D

onready var label = $Area2D/Label
onready var boar1 = $BoarBody/Area2D/CollisionShape2D2
onready var boar2 = $BoarBody2/Area2D/CollisionShape2D2
onready var boar1body = $BoarBody
onready var boar2body = $BoarBody2
onready var hackLabel = $Label

func _ready():
	if Stats.boar1_dead == true:
		boar1body.hide()
		boar1.disabled = true
	if Stats.boar2_dead == true:
		boar2body.hide()
		boar2.disabled = true
	label.hide()
	if Stats.loop >= 10:
		hackLabel.show()
	else:
		hackLabel.hide()

func _on_Dragon_body_entered(body):
	if body.name == "MainChar":
		get_tree().get_root().get_node("Node2D/DragonBody").hide()
		#Stats._change_scene("MainGame", "DragonBattle")
		get_tree().change_scene("res://Scenes/DragonBattle.tscn")

func _on_Boar_body_entered(body):
	if body.name == "MainChar":
		Stats.boar1_dead = true
		#Stats._change_scene("MainGame", "BoarBattle")
		get_tree().change_scene("res://Scenes/BoarBattle.tscn")

func _on_Boar2_body_entered(body):
	if body.name == "MainChar":
		Stats.boar2_dead = true
		#Stats._change_scene("MainGame", "BoarBattle")
		get_tree().change_scene("res://Scenes/BoarBattle.tscn")

func _on_Sign_body_entered(body):
	if body.name == "MainChar":
		label.show()

func _on_Sign_body_exited(body):
	if body.name == "MainChar":
		label.hide()

func _on_Area2D2_body_entered(body):
	if body.name == "MainChar":
		get_tree().change_scene("res://Scenes/Loop.tscn")
