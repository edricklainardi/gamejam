extends ColorRect

func _ready():
	Stats.power += 5
	Stats.heal_amount += 2
	Stats.max_hp += 10

func _on_LinkButton_pressed():
	get_tree().change_scene("res://Scenes/DragonBattle.tscn")
