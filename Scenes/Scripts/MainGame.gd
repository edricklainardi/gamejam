extends Node2D

onready var label = $Area2D/Label
onready var boar1 = $BoarBody
onready var boar2 = $BoarBody2

func _ready():
	if Stats.boar1_dead == true:
		boar1.remove_child()
	if Stats.boar2_dead == true:
		boar2.remove_child()
	label.hide()

func _on_Dragon_body_entered(body):
	if body.name == "MainChar":
		get_tree().get_root().get_node("Node2D/DragonBody").hide()
		#Stats._change_scene("MainGame", "DragonBattle")
		get_tree().change_scene("res://Scenes/DragonBattle.tscn")

func _on_Boar_body_entered(body):
	if body.name == "MainChar":
		Stats.boar1_dead = true
		#Stats._change_scene("MainGame", "BoarBattle")
		get_tree().change_scene("res://Scenes/BoarBattle.tscn")

func _on_Boar2_body_entered(body):
	if body.name == "MainChar":
		Stats.boar2_dead = true
		#Stats._change_scene("MainGame", "BoarBattle")
		get_tree().change_scene("res://Scenes/BoarBattle.tscn")

func _on_Sign_body_entered(body):
	if body.name == "MainChar":
		label.show()

func _on_Sign_body_exited(body):
	if body.name == "MainChar":
		label.hide()
