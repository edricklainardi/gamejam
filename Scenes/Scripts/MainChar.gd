extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()
var has_double_jumped = false

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
		has_double_jumped = false
	if not is_on_floor() and Input.is_action_just_pressed('ui_up') and has_double_jumped == false:
		velocity.y = jump_speed
		has_double_jumped = true
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed

func _input(event):
	velocity.x = 0
	if event is InputEventKey:
		if event.scancode == KEY_SPACE:
			dash()
"""
		if event.scancode == KEY_UP and is_on_floor():
			velocity.y = jump_speed
			has_double_jumped = false
		if event.scancode == KEY_UP and not is_on_floor() and has_double_jumped == false:
			velocity.y = jump_speed
			has_double_jumped = true
		if event.scancode == KEY_RIGHT:
			velocity.x += speed
		if event.scancode == KEY_LEFT:
			velocity.x -= speed
"""
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func dash():
	speed = 800
	$Timer.start()

func _on_Timer_timeout():
	speed = 400
