extends Node

const BattleUnits = preload("res://Assets/Resources/BattleUnits.tres")

onready var proceedButton = $UI/CenterContainer/ProceedButton
onready var battleButtons = $UI/BattleButtons

func _ready():
	proceedButton.hide()
	start_player_turn()
	var enemy = BattleUnits.Enemy
	if enemy != null:
		enemy.connect("dead", self, "_on_Enemy_dead")

func start_enemy_turn():
	battleButtons.hide()
	var enemy = BattleUnits.Enemy
	if enemy != null:
		enemy.attack()
		yield(enemy, "end_turn")
	start_player_turn()

func start_player_turn():
	battleButtons.show()
	var playerStats = BattleUnits.PlayerStats
	if playerStats.hp == 0:
		get_tree().change_scene("res://Scenes/Dead.tscn")
	playerStats.ap = playerStats.max_ap
	yield(playerStats, "end_turn")
	start_enemy_turn()

func _on_ProceedButton_pressed():
	get_tree().change_scene("res://Scenes/WinScene.tscn")

func _on_Enemy_dead():
	proceedButton.show()
	battleButtons.hide()
